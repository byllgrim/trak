#!/usr/bin/env wish

namespace eval trak {
    variable usec1 0
    variable usec2 2000000
}

proc usec2sec {usec} {
    return [expr $usec * 1.0 / 1000000]
}

proc addclip {line} {
    set id [lindex $line 0]
    set clip [string cat ".clip" $id]
    destroy $clip
    frame $clip -background grey -borderwidth 1 -relief solid
    place $clip -relheight 1
    lower $clip

    set fname [lindex $line 1]
    label $clip.file -text $fname
    place $clip.file -x 0 -y 0

    set pos [lindex $line 2]
    set rx [expr 1.0 * $pos / [expr $::trak::usec2 - $::trak::usec1]]
    place $clip -relx $rx

    set len [lindex $line 3]
    set rw [expr $len / 1000000. / 10.]
    place $clip -relwidth $rw
}

proc drawplayhead {pos} {
    set paststart [expr $pos - $::trak::usec1]
    set view [expr $::trak::usec2 - $::trak::usec1]
    set ratio [expr 1.0 * $paststart / $view]
    set screenpos [expr $ratio * [winfo width .]]

    destroy .playhead
    frame .playhead -background red
    place .playhead -relheight 1 -width 1 -x $screenpos
}

proc readstdin {} {
    set line [split [gets stdin]]

    if {[string compare "playhead" [lindex $line 0]] == 0} {
        drawplayhead [lindex $line 1]
        return 0;
    }

    addclip $line
}

proc wait4key {key} {
    set cmd [bind . $key]

    set done 0
    bind . $key {set done 1}
    vwait done

    bind . $key $cmd
}

proc move {} {
    set x [winfo pointerx .]
    set y [winfo pointery .]

    wait4key m
    set x2 [expr [winfo pointerx .] - [winfo x .]]
    set ratio [expr 1.0 * $x2 / [winfo width .]]
    set viewrange [expr ($::trak::usec2 - $::trak::usec1)]
    set pos [expr $ratio * $viewrange]

    set id [winfo containing $x $y]
    set id [regsub -all "\\." $id ""]
    set id [regsub -all "\[a-z\]*" $id ""]

    puts [concat "move" $id $pos]
}

proc main {} {
    label .timeframe -text "start:end" -fg white -bg black
    pack .timeframe
    set tftext [format "%f:%f" [usec2sec $::trak::usec1] [usec2sec $::trak::usec2]]
    .timeframe configure -text $tftext

    bind . a {puts [concat "add " [tk_getOpenFile]]}
    bind . <space> {puts "play"}
    bind . q {puts "quit"; exit}
    bind . m {move}

    fconfigure stdin -blocking 0
    fileevent stdin readable readstdin
}

main
