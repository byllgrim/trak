#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <samplerate.h>
#include <sndfile.h>
#include <jack/jack.h>

#include "trak.h"
#include "util.h"



enum {BUFLEN = 1*1000*1000};

struct context {
  jack_client_t  *jc;
  jack_port_t    *jp;
  float           samples[BUFLEN];  // TODO different size
  SF_INFO         si;
  uint64_t        delay;
};



static int terminate = 0;



static void
setdelay(struct context *ctx, char *str)
{
  char           *endptr;
  uint64_t        usec;
  uint64_t        samples;
  jack_nframes_t  samplerate;

  usec = strtoll(str, &endptr, 10);
  if (endptr == str)
    return;

  samplerate = jack_get_sample_rate(ctx->jc);
  samples    = (samplerate * usec) / 1000000;  // TODO which order?
  ctx->delay = samples;
}

static void
fixsamplerate(struct context *ctx)
{
  SRC_DATA  conf;
  float     obuf[BUFLEN];
  size_t    orate;
  size_t    newsize;
  int       err;

  if (ctx->si.frames == 0)
    return;

  orate              = jack_get_sample_rate(ctx->jc);
  conf.data_in       = ctx->samples;
  conf.input_frames  = ctx->si.frames;
  conf.data_out      = obuf;
  conf.output_frames = LEN(obuf) / ctx->si.channels;
  conf.src_ratio     = (float)orate / ctx->si.samplerate;
  err = src_simple(&conf, SRC_SINC_MEDIUM_QUALITY, ctx->si.channels);
  if (err) {
    return;
  }

  newsize = conf.output_frames_gen * ctx->si.channels * sizeof(*ctx->samples);
  memset(ctx->samples, 0, sizeof(ctx->samples));
  memcpy(ctx->samples, obuf, newsize);
  ctx->si.frames     = conf.output_frames_gen;
  ctx->si.samplerate = orate;
  // TODO be more careful with these sizes
  // TODO refactor
}

static void
docmd(char *str, struct context *ctx)
{
  char* cmd;
  char* arg;

  cmd = strtok(str, " \n\t");
  arg = strtok(0, " \n\t");
  if (!cmd)
    return;

  if (strcmp(cmd, "load") == 0) {
    loadfile(arg, ctx->samples, LEN(ctx->samples), &ctx->si);
    fixsamplerate(ctx);
  }
  if (strcmp(cmd, "delay") == 0) {
    setdelay(ctx, arg);
  }
  // TODO table lookup instead of if ladder?
}

static void
trans2buf(
  jack_default_audio_sample_t *buf,
  jack_nframes_t               nframes,
  struct context              *ctx,
  jack_position_t              pos)
{
  size_t  b1, b2, bn;  // buffer frame pos
  size_t  s1, s2, sn;  // sample frame pos
  size_t  f1, f2, fn;  // frame position

  b1 = pos.frame;
  s1 = ctx->delay;
  f1 = MAX(b1, s1);

  b2 = b1 + nframes;
  s2 = s1 + ctx->si.frames;
  f2 = MIN(b2, s2);

  bn = f1 - b1;
  sn = f1 - s1;
  for (fn = f1; fn < f2; fn++) {
    if (bn >= nframes)
      return;
    if (sn >= (size_t)ctx->si.frames)
      return;

    buf[bn] = ctx->samples[sn * ctx->si.channels];
    bn++;
    sn++;
  }
}

static int
processjack(jack_nframes_t nframes, void *arg)
{
  jack_default_audio_sample_t  *outbuf;
  struct context               *ctx;
  jack_position_t               pos;
  jack_transport_state_t        state;

  ctx    = (struct context *)arg;
  outbuf = getoutbuffer(ctx->jp, nframes);
  state  = jack_transport_query(ctx->jc, &pos); // TODO check valid flag
  if (state != JackTransportRolling)
    return 0;

  // TODO check time range before wasting time
  trans2buf(outbuf, nframes, ctx, pos);

  return 0;
}

static void
handlesignal(int signum)
{
  (void)signum;
  terminate = 1;
}

static void
setsighandlers(void)
{
  struct sigaction  sa;

  memset(&sa, 0, sizeof(sa));
  sigemptyset(&sa.sa_mask);  // TODO why?

  sa.sa_handler = handlesignal;
  sigaction(SIGINT, &sa, 0);
  sigaction(SIGQUIT, &sa, 0);
  sigaction(SIGTERM, &sa, 0);
}

int
main(void)
{
  char            cmd[1000];
  struct context  ctx;

  memset(&ctx, 0, sizeof(ctx));
  ctx.jc = eopenjack();
  ctx.jp = ecreatejport(ctx.jc);
  jack_set_process_callback(ctx.jc, processjack, &ctx);
  jack_activate(ctx.jc);
  setsighandlers();

  for (; !terminate;) {
    fgets(cmd, sizeof(cmd), stdin);
    if (cmd[0] == 'q')
      break;
    docmd(cmd, &ctx);
  }

  jack_client_close(ctx.jc);
  return EXIT_SUCCESS;
}
