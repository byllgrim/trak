The "master" coordinates the "players".
It is responsible for reading a track's text file.
Then it spawns the necessary players to handle each audio clip.
