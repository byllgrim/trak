#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <unistd.h>

#include "util.h"



enum {
  LINELEN = 1024,
  LISTLEN = 10,  // TODO rename
};

struct entry {  // TODO better name
    char filepath[LINELEN];
    char position[LINELEN];
    char preskip[LINELEN];
    char postskip[LINELEN];
    // TODO non-static in the future
};



static
void
removenewline(char *buf)
{
    char *nl;

    for (; (nl = strchr(buf, '\n')); )
        *nl = 0;
}

static
void
printentry(struct entry *entry)
{
    printf("filepath '%s'\n", entry->filepath);
    printf("position '%s'\n", entry->position);
    printf("preskip  '%s'\n", entry->preskip);
    printf("postskip '%s'\n", entry->postskip);
}

static
void
parseentry(char *buf, struct entry *entry)
{
    char *toks[4];
    int   i;

    removenewline(buf);
    memset(entry, 0, sizeof(*entry));

    toks[0] = strtok(buf, " ");
    if (!toks[0])
        return;  // TODO sufficient cleanup?
    for (i = 1; i < 4; i++) {
        toks[i] = strtok(0, " ");
        if (!toks[i])
            toks[i] = "";
    }
    strncpy(entry->filepath, toks[0], (LINELEN - 1));
    strncpy(entry->position, toks[1], (LINELEN - 1));
    strncpy(entry->preskip,  toks[2], (LINELEN - 1));
    strncpy(entry->postskip, toks[3], (LINELEN - 1));
}

static
void
readfile(char *filename, struct entry entrylist[LISTLEN])  // TODO ok?
{
    FILE *fp;
    char  buf[LINELEN];
    int   i;

    fp = fopen(filename, "r");
    if (!fp)
        die("can't open file '%s'\n", filename);  // TODO dangerous to die?!

    memset(entrylist, 0, (sizeof(*entrylist) * LISTLEN));  // TODO efficiency
    for (i = 0; i < LISTLEN; i++) {
        if (!fgets(buf, sizeof(buf), fp))
            break;

        parseentry(buf, &entrylist[i]);
    }

    fclose(fp);
}

static
int
isvalidentry(struct entry *entry)
{
    return !!entry->filepath[0];
}

static
void
printentries(struct entry entrylist[LISTLEN])
{
    int           i;
    struct entry *entry;

    for (i = 0; i < LISTLEN; i++) {
        entry = &entrylist[i];
        if (isvalidentry(entry))
            printentry(entry);
    }
}

static
void
setsigterm(void (*handler)(int))
{
    struct sigaction  sa;

    memset(&sa, 0, sizeof(sa));
    sigemptyset(&sa.sa_mask);  // TODO why?

    sa.sa_handler = handler;
    sigaction(SIGTERM, &sa, 0);
}

static
void
spawnentry(struct entry *entry)
{
  FILE  *fp;

  setsigterm(SIG_DFL);
  fp = popen("trak-player", "w");
  setsigterm(SIG_IGN);  // TODO cleaner way?
  if (!fp)
    return;

  fprintf(fp, "load %s\n", entry->filepath);
  fprintf(fp, "delay %s\n", entry->position);
  // TODO preskip and postskip
  fprintf(fp, "info\n");
  fflush(fp);
}

static
void
spawnchilds(struct entry entrylist[LISTLEN])
{
    int           i;
    struct entry *entry;

    for (i = 0; i < LISTLEN; i++) {
        entry = &entrylist[i];
        if (isvalidentry(entry))
            spawnentry(entry);
    }
}

static
void
killallchildren(void)
{
    pid_t  pid;

    pid = getpid();
    killpg(pid, SIGTERM);
    // TODO ensure childs in correct group
    // TODO check if kill succeeded
}

static
void
handlesignal(int signum)
{
    (void)signum;

    killallchildren();
    // TODO also atexit()?

    exit(EXIT_SUCCESS);
}

static
void
setsighandlers(void)
{
    struct sigaction  sa;

    memset(&sa, 0, sizeof(sa));
    sigemptyset(&sa.sa_mask);  // TODO why?

    sa.sa_handler = handlesignal;
    sigaction(SIGINT, &sa, 0);
    sigaction(SIGQUIT, &sa, 0);
    // TODO check for errors?
}

static
int
estartmonitoring(char *filepath)
{
    int  fd;
    int  wd;

    fd = inotify_init();
    if (fd < 0)
        die("inotify failed to init\n");

    wd = inotify_add_watch(fd, filepath, IN_MODIFY);
    if (wd < 0)
        die("inotify failed to add watch '%s'\n", filepath);

    // TODO also monitor audio files in track?
    return fd;
}



int
main(int argc, char *argv[])
{
    struct entry          entrylist[LISTLEN];
    int                   fd;
    struct inotify_event  ie;

    if (argc != 2)
        die("usage: %s <trakfile>\n", argv[0]);

    fd = estartmonitoring(argv[1]);
    setsighandlers();
    readfile(argv[1], entrylist);
    printentries(entrylist);
    spawnchilds(entrylist);

    for (;;) {
        read(fd, &ie, sizeof(ie));  // This blocks
        // TODO prevent read from going twice at each change
        killallchildren();
        readfile(argv[1], entrylist);  // TODO clean it first?
        spawnchilds(entrylist);
    }

    return EXIT_FAILURE;
}
