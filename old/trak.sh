#!/usr/bin/env sh

mkfifo server.in server.out
./trak-cli > server.out < server.in &
./trak-gui.tcl < server.out > server.in
rm server.in server.out
