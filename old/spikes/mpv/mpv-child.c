#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
    FILE *mpv;
    char  cmd[1000] = {0};

    if (argc <= 1) {
        fprintf(stderr, "usage: %s <audiofile>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    mpv = popen("mpv --idle --input-file=/dev/stdin", "w");

    snprintf(cmd, (sizeof(cmd) - 1), "loadfile %s\n", argv[1]);
    fprintf(mpv, cmd);
    fflush(mpv);

    sleep(2);
    snprintf(cmd, (sizeof(cmd) - 1), "quit\n");
    fprintf(mpv, cmd);
    fflush(mpv);

    pclose(mpv);
    return EXIT_SUCCESS;
}
