#include <stdio.h>
#include <stdlib.h>
#include <mpv/client.h>

int
main(int argc, char *argv[])
{
    mpv_handle *ctx;
    mpv_event  *event;
    char        args[1000] = {0};

    if (argc <= 1) {
        fprintf(stderr, "usage: %s <audiofile>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    ctx = mpv_create();
    mpv_initialize(ctx);

    snprintf(args, (sizeof(args) - 1), "loadfile %s", argv[1]);
    mpv_command_string(ctx, args);

    for (;;) {
        event = mpv_wait_event(ctx, 60);
        printf("event: %s\n", mpv_event_name(event->event_id));
        if (event->event_id == MPV_EVENT_IDLE)
            break;
    }

    mpv_terminate_destroy(ctx);
    return EXIT_SUCCESS;
}
