#include <jack/jack.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

FILE *mpv;  // TODO no globals

void
die(char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    exit(EXIT_FAILURE);
}

void
mpvprintf(FILE *mpv, char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    vfprintf(mpv, fmt, ap);
    va_end(ap);

    fflush(mpv);
}

int
process(jack_nframes_t nf, void *client)
{
    jack_position_t pos;
    int seconds;

    (void)nf;

    jack_transport_query(client, &pos);
    seconds = pos.frame / pos.frame_rate;
    if (seconds > 10) {
        printf("playing at %d\n", seconds);
        mpvprintf(mpv, "set pause no\n");
        sleep(5);
        exit(EXIT_SUCCESS);
    }

    return 0;
}

int
main(int argc, char *argv[])
{
    jack_client_t *jc;

    if (argc < 2)
        die("usage: %s <audiofile>\n", argv[0]);

    jc = jack_client_open("jack-client", JackNoStartServer, 0);
    if (!jc)
        die("failed to open jack client\n");

    mpv = popen("mpv --idle --pause --input-file=/dev/stdin", "w");
    mpvprintf(mpv, "loadfile %s\n", argv[1]);

    jack_set_process_callback(jc, process, jc);

    if (jack_activate(jc))
        die("can't activate client\n");

    for (;;)
        sleep(1);

    mpvprintf(mpv, "quit\n");
    fclose(mpv);
    return EXIT_SUCCESS;
}
