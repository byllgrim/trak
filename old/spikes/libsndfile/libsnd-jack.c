#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jack/jack.h>
#include <sndfile.h>



enum {SNDBUFLEN = 1*1000*1000};

struct processdata {
  float       *buf;
  sf_count_t   itemslen;
  jack_port_t *port;
};



static
char *
egetfilepath(int argc, char **argv)
{
  if (argc != 2) {
    fprintf(stderr, "usage: %s <audiofile>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  return argv[1];
}

static
SNDFILE *
eopenfile(char *filepath, SF_INFO *sfinfo)
{
  SNDFILE *sndfile;

  memset(sfinfo, 0, sizeof(*sfinfo));
  sndfile = sf_open(filepath, SFM_READ, sfinfo);
  if (!sndfile) {
    fprintf(stderr, "error eopenfile()\n");
    exit(EXIT_FAILURE);
  }

  return sndfile;
}

static
sf_count_t
ereadfile(SNDFILE *sndfile, SF_INFO *sfinfo, float *buf, int buflen)
{
  sf_count_t  neededlen;
  sf_count_t  vacancylen;
  sf_count_t  rlen;

  neededlen = sfinfo->frames * sfinfo->channels;
  if (buflen < neededlen) {
    fprintf(stderr, "error sound buffer too small\n");
    exit(EXIT_FAILURE);
  }

  vacancylen = buflen / sfinfo->channels;
  rlen = sf_read_float(sndfile, buf, vacancylen);

  // TODO get samplerate correct
  return rlen;
}

static
jack_client_t *
eopenjack()
{
  jack_client_t *jc;

  jc = jack_client_open("libsnd-jack", 0, 0);
  if (!jc) {
    fprintf(stderr, "error jack_client_open\n");
    exit(EXIT_FAILURE);
  }

  return jc;
}

static
int
processjack(jack_nframes_t nframes, void *arg)
{
  static int                   pos = 0;
  size_t                       i;
  jack_default_audio_sample_t *outbuf;
  jack_port_t                 *outport;
  struct processdata          *pd;
  sf_count_t                   itemslen;
  float                       *buf;

  pd       = (struct processdata *)arg;
  buf      = pd->buf;
  itemslen = pd->itemslen;
  outport  = pd->port;
  outbuf   = jack_port_get_buffer(outport, nframes);

  for (i = 0; i < nframes; i++) {
    outbuf[i] = buf[pos];
    pos++;
    pos %= itemslen;
  }

  return 0;
}

static
jack_port_t *
ecreateport(jack_client_t *jc)
{
  jack_port_t *p;

  p = jack_port_register(
    jc,
    "outport",
    JACK_DEFAULT_AUDIO_TYPE,
    JackPortIsOutput,
    0
    );
  if (!p) {
    fprintf(stderr, "error jack_port_register");
    exit(EXIT_FAILURE);
    // TODO some cleanup first?
  }

  return p;
}



int
main(int argc, char **argv)
{
  char               *filepath;
  SNDFILE            *sndfile;
  SF_INFO             sfinfo;
  float               buf[SNDBUFLEN];
  jack_client_t      *jc;
  jack_port_t        *port;
  struct processdata  pd;
  sf_count_t          itemslen;

  filepath = egetfilepath(argc, argv);
  sndfile  = eopenfile(filepath, &sfinfo);
  itemslen = ereadfile(sndfile, &sfinfo, buf, SNDBUFLEN);

  jc          = eopenjack();
  port        = ecreateport(jc);
  pd.buf      = buf;
  pd.itemslen = itemslen;
  pd.port     = port;
  jack_set_process_callback(jc, processjack, &pd);
  jack_activate(jc);

  for (; getchar() != 'q'; )
    ;

  sf_close(sndfile);
  jack_client_close(jc);
  return EXIT_FAILURE;
}
