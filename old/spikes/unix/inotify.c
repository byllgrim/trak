#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <unistd.h>

int
main(void)
{
  int                    fd;
  int                    wd;
  char                   buf[1000];
  struct inotify_event  *ie;

  fd = inotify_init();
  if (fd < 0)
    exit(EXIT_FAILURE);

  wd = inotify_add_watch(fd, ".", IN_MODIFY);
  if (wd < 0)
    exit(EXIT_FAILURE);

  read(fd, buf, sizeof(buf));
  ie = (struct inotify_event *)buf;
  printf("event on '%s'\n", ie->name);

  close(fd);
  return EXIT_SUCCESS;
}
