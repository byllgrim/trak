#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
main(void)
{
    fd_set readfds = {0};
    char   buf[500] = {0};

    FD_SET(STDIN_FILENO, &readfds);

    for (;;) {
        select((STDIN_FILENO + 1), &readfds, 0, 0, 0);
        read(STDIN_FILENO, buf, 200);
        printf("stdin: %s", buf);
    }

    return EXIT_FAILURE;
}
