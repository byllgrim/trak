#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

static
FILE *
popenpid(char *cmd, pid_t *pid)
{
  int    err;
  int    ischild;
  int    pipefd[2];
  FILE  *fp;

  err = pipe(pipefd);
  if (err)
    return 0;

  *pid = fork();
  if (*pid < 0)
    return 0;

  ischild  = (*pid == 0);
  if (ischild) {
    dup2(pipefd[0], STDIN_FILENO);
    execlp(cmd, cmd, (void *)0);
    exit(EXIT_FAILURE);
  }

  fp = fdopen(pipefd[1], "w");
  if (!fp)
    return 0;  // how 'bout cleanup?

  return fp;
}

int
main(void)
{
  FILE  *fwrite;
  pid_t  pid;

  fwrite = popenpid("ed", &pid);

  fprintf(fwrite, "a\n");
  fprintf(fwrite, "Hello, child, from parent\n");
  fprintf(fwrite, ".\n");
  fprintf(fwrite, "w out\n");
  fprintf(fwrite, "q\n");
  fflush(fwrite);

  alarm(10);
  wait(0);
  fclose(fwrite);
  kill(pid, SIGTERM);
  return EXIT_SUCCESS;
}
