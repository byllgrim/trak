#include <signal.h>
#include <stdio.h>
#include <string.h>

static void
handlesignal(int signum)
{
  (void)signum;

  printf("HANDLING SIGNAL\n");
}

int
main(void)
{
  struct sigaction  sa;

  memset(&sa, 0, sizeof(sa));
  sigemptyset(&sa.sa_mask);

  sa.sa_handler = handlesignal;
  sigaction(SIGINT, &sa, 0);
  sigaction(SIGQUIT, &sa, 0);
  sigaction(SIGTERM, &sa, 0);

  for (; getchar() != 'q'; )
    ;

  return 0;
}
