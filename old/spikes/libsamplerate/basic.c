#include <stdio.h>
#include <stdlib.h>

#include <samplerate.h>

int
main(void)
{
  float     ibuf[1000] = {0};
  float     obuf[1000] = {0};
  SRC_DATA  conf       = {0};
  int       res        = {0};

  conf.data_in       = ibuf;
  conf.input_frames  = 1000;
  conf.data_out      = obuf;
  conf.output_frames = 1000;
  conf.src_ratio     = (float)44100 / 48000;

  res = src_simple(&conf, SRC_SINC_MEDIUM_QUALITY, 1);
  printf("did '%s'\n", src_strerror(res));
  printf("in used %li\n", conf.input_frames_used);
  printf("out gen %li\n", conf.output_frames_gen);

  return EXIT_SUCCESS;
}
