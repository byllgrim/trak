#include <stdio.h>
#include <stdlib.h>

#include <jack/jack.h>

int
main(void)
{
    jack_client_t *jc;

    jc = jack_client_open("jack-client", JackNoStartServer, 0);
    if (!jc) {
        fprintf(stderr, "failed to open jack client\n");
        exit(EXIT_FAILURE);
    }
    printf("Hello, Jack!\n");

    jack_client_close(jc);
    return EXIT_SUCCESS;
}
