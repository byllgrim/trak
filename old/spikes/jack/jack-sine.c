#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <jack/jack.h>



static
jack_client_t *
eopenjack()
{
  jack_client_t *jc;

  jc = jack_client_open("jack-sine", 0, 0);
  if (!jc) {
    fprintf(stderr, "error jack_client_open\n");
    exit(EXIT_FAILURE);
  }

  return jc;
}

static
int
processjack(jack_nframes_t nframes, void *arg)
{
  static int                   pos = 0;
  size_t                       i;
  jack_default_audio_sample_t *outbuf;
  jack_port_t                 *outport;

  outport = (jack_port_t *)arg;
  outbuf  = jack_port_get_buffer(outport, nframes);

  for (i = 0; i < nframes; i++) {
    outbuf[i] = 0.1 * sin(2 * M_PI * ((float)pos / 200));
    pos++;
  }

  return 0;
}

static
jack_port_t *
ecreateport(jack_client_t *jc)
{
  jack_port_t *p;

  p = jack_port_register(
    jc,
    "outport",
    JACK_DEFAULT_AUDIO_TYPE,
    JackPortIsOutput,
    0
    );
  if (!p) {
    fprintf(stderr, "error jack_port_register");
    exit(EXIT_FAILURE);
    // TODO some cleanup first?
  }

  return p;
}



int
main(void)
{
  jack_client_t *jc;
  jack_port_t   *port;

  jc   = eopenjack();
  port = ecreateport(jc);
  jack_set_process_callback(jc, processjack, port);
  jack_activate(jc);

  for (; getchar() != 'q'; )
    ;

  jack_client_close(jc);
  return EXIT_SUCCESS;
}
