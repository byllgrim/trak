#include <jack/jack.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void
die(char *msg)
{
    fprintf(stderr, msg);
    exit(EXIT_FAILURE);
}

int
process(jack_nframes_t nf, void *client)
{
    static int seconds = -1;
    jack_position_t pos;
    int newsec;

    (void)nf;

    jack_transport_query(client, &pos);
    newsec = pos.frame / pos.frame_rate;
    if (newsec != seconds) {
        seconds = newsec;
        printf("%d\n", seconds);
    }

    // TODO only if pos.valid

    return 0;
}

int
main(void)
{
    jack_client_t *jc;

    jc = jack_client_open("jack-client", JackNoStartServer, 0);
    if (!jc)
        die("failed to open jack client\n");

    jack_set_process_callback(jc, process, jc);

    if (jack_activate(jc))
        die("can't activate client\n");

    // TODO register shutdown code?
    // TODO set signal handlers for exiting?

    for (;;)
        sleep(1);

    jack_client_close(jc);
    return EXIT_SUCCESS;
}
