#!/usr/bin/env wish

# All time values are in micro seconds

namespace eval track {
	variable t0 0         ; # Start of viewing range
	variable t1 60000000  ; # End of viewing range
	variable clips        ; # Dict of all the track's clips
	# TODO keep these in main instead of this global?
}

proc setClip {id pos len file} {
	# Fill a "struct" with relevant information
	dict set clip id $id
	dict set clip pos $pos
	dict set clip len $len
	dict set clip file $file

	# Add the clip to the list of all clips
	set clipName [string cat "clip" $id] ; # TODO is prefix necessary?
	dict set track::clips $clipName $clip

	# Update graphics with the new clip
	drawClip $clip  ; # TODO responsibility belong here? Use virtual events?
}

proc setTimeFrame {t0 t1} {
	# TODO assert correct input format

	set track::t0 $t0
	set track::t1 $t1

	drawTimeDials
	drawAllClips
}

proc drawClip {clip} {
	# Use id to create pathName for a labelframe
	set id [dict get $clip id]
	set framePath [string cat ".clip" $id]
	if {[winfo exists $framePath] == 0} {
		labelframe $framePath
	}

	# Use pos to calculate relative x-coordinates
	set pos [dict get $clip pos]
	set relStart [expr 1.0 * ($pos - $track::t0) / ($track::t1 - $track::t0)]
	place $framePath -relx $relStart

	# Use len to set visual width of clip
	set len [dict get $clip len]
	set relWidth [expr 1.0 * $len / ($track::t1 - $track::t0)]
	place $framePath -relwidth $relWidth

	# Use file to set the label of the clip
	set file [dict get $clip file]
	$framePath configure -text $file

	# Cosmetic options independent of the procedure arguments
	$framePath configure -background #073642
	$framePath configure -foreground #839496
	$framePath configure -padx 0 -pady 0
	$framePath configure -borderwidth 1 -relief solid  ; # TODO same color as text
	place $framePath -rely 0.00
	place $framePath -relheight 1.00
	lower $framePath
}

proc drawTimeDials {} {
	# Create gui labels to show time dials
	destroy .t0  ; # TODO just don't create if existing
	destroy .t1
	label .t0  ; # TODO better name?
	label .t1
	.t0 configure -text $track::t0  ; # TODO nicer representation than us
	.t1 configure -text $track::t1

	# Place the labels on screen
	pack .t0 -side left -anchor s
	pack .t1 -side right -anchor s

	# Customize colors
	.t0 configure -background #002b36  ; # TODO base03
	.t1 configure -background #002b36
	.t0 configure -foreground #586e75  ; # TODO base01
	.t1 configure -foreground #586e75
}

proc drawAllClips {} {
	foreach clipName [dict keys $track::clips] {
		set clip [dict get $track::clips $clipName]
		drawClip $clip
		# TODO don't draw when neither start/stop is within time frame
	}
}

proc scroll {direction} {
	set relAmount 0.10
	set viewRange [expr $track::t1 - $track::t0]
	set delta [expr $relAmount * $viewRange]

	switch $direction {
		left {
			set delta [expr -1 * $delta]
		}
		right {
			# Do nothing
		}
		default {
			return
		}
	}

	set t0 [expr $track::t0 + $delta]
	set t1 [expr $track::t1 + $delta]
	setTimeFrame $t0 $t1
}

proc initUserInput {} {
	bind . <Button-4> {scroll left}
	bind . <Button-5> {scroll right}
	# TODO this ^ isn't cross platform?
}

proc main {} {
	# Setting main window colors
	. configure -background #002b36  ; # TODO refactor

	# TODO ?
	initUserInput

	# Testing the API
	setClip 0 1000000 5000000 "test clip"
	setClip 1 10000000 20000000 "second clip"
	setTimeFrame 2000000 35000000
	setTimeFrame -1000000 2000000
	setTimeFrame 0 31000000
	# TODO refactor/remove
}

main
