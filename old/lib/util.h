#define  LEN(X)     (sizeof(X) / sizeof(X)[0])
#define  MIN(A, B)  ((A) < (B) ? (A) : (B))
#define  MAX(A, B)  ((A) > (B) ? (A) : (B))

void die(const char *fmt, ...);
