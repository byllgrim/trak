jack_client_t*               eopenjack    (void);
jack_port_t*                 ecreatejport (jack_client_t *jc);
void                         loadfile     (char *fpath, float *buf, size_t maxlen, SF_INFO *si);
jack_default_audio_sample_t* getoutbuffer (jack_port_t *jp, jack_nframes_t nframes);
