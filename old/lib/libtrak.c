#include <jack/jack.h>
#include <sndfile.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "util.h"

jack_client_t *
eopenjack(void)
{
  jack_client_t  *jc;

  jc = jack_client_open("trak", 0, 0);
  if (!jc)
    die("error: can't open jack");

  // TODO cleanup later
  return jc;
}

jack_port_t *
ecreatejport(jack_client_t *jc)
{
  jack_port_t *p;

  p = jack_port_register(
    jc,
    "outport",
    JACK_DEFAULT_AUDIO_TYPE,
    JackPortIsOutput,
    0
    );
  if (!p)
    die("error: failed to create jack port\n");
  // TODO some cleanup before exit?

  // TODO stereo channels (or more)
  return p;
}

void
loadfile(char *fpath, float *buf, size_t maxlen, SF_INFO *si)
{
  SNDFILE  *sf;

  if (!fpath)
    return;

  memset(si, 0, sizeof(*si));
  sf = sf_open(fpath, SFM_READ, si);
  if (!sf) {
    memset(si, 0, sizeof(*si));
    // TODO check more errors?
    return;
  }

  memset(buf, 0, (maxlen * sizeof(*buf)));
  sf_read_float(sf, buf, maxlen);
  // TODO test if buf is large enough? malloc?

  // TODO clean and stuff
  // TODO shell ~ et al
  // TODO continue to silently ignore errors?
}

jack_default_audio_sample_t *
getoutbuffer(jack_port_t *jp, jack_nframes_t nframes)
{
  jack_default_audio_sample_t  *outbuf;

  outbuf  = jack_port_get_buffer(jp, nframes);
  memset(outbuf, 0, (nframes * sizeof(*outbuf))); // TODO don't always?

  return outbuf;
}
