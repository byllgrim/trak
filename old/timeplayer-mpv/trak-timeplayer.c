#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <jack/jack.h>
#include <mpv/client.h>



enum {
    PATHSIZ = 1024,
    KEYSIZ = 128,
    VALSIZ = 1024
    // TODO is this cool?
};

struct audioclip { // TODO rename
    char      filepath[PATHSIZ];
    uint64_t  position;  // TODO startusec;
    uint64_t  startoffset;  // TODO bypassusec;
    uint64_t  endoffset;  // TODO aversionusec;
    // TODO preskip/postskip? startskip/endtrim? length?
};

struct keyval {
    char key[KEYSIZ];
    char val[VALSIZ];
};

struct mpv {
    mpv_handle *handle;
    int         playing;
};

struct context {
    struct audioclip  audioclip;
    jack_client_t    *jackclient;
    struct mpv        mpv;
};



static
void
die(char *msg)
{
    fprintf(stderr, msg);
    exit(EXIT_FAILURE);
}

static
void
waitforinput(void)
{
    fd_set readfds = {0};

    FD_SET(STDIN_FILENO, &readfds);
    select((STDIN_FILENO + 1), &readfds, 0, 0, 0);
}

static
int
readinput(struct keyval *kv)
{
    char  buf[BUFSIZ];
    char *key;
    char *val;
    char *nl;
    char *fret;

    fret = fgets(buf, BUFSIZ, stdin);
    if (!fret)
        return -1;
    if ( (nl = strchr(buf, '\n')) )
        *nl = 0;

    key = strtok(buf, " ");
    val = strtok(0, " ");

    if (key)
        strncpy(kv->key, key, (KEYSIZ - 1));
    if (val)
        strncpy(kv->val, val, (VALSIZ - 1));

    return 0;
}

static
void
printaudioclip(struct audioclip *ac)
{
    printf("filepath '%s'\n", ac->filepath);
    printf("position %llu\n", ac->position);
    // TODO rest of them
}

static
void
updatefilepath(struct context *ctx, struct keyval *kv)
{
    struct audioclip *ac;
    char args[PATHSIZ + 100] = {0};
    mpv_handle *mpv;

    ac = &ctx->audioclip;
    mpv = ctx->mpv.handle;

    strncpy(ac->filepath, kv->val, (PATHSIZ - 1));
    snprintf(args, (sizeof(args) - 1), "loadfile %s", ac->filepath);
    mpv_command_string(mpv, args);
}

static
void
updateposition(struct audioclip *ac, struct keyval *kv)
{
    uint64_t newpos;
    char *endptr;

    newpos = strtoll(kv->val, &endptr, 10);

    if (endptr != kv->val)
        ac->position = newpos;
}

static
void
quit(struct context *ctx)
{
    jack_client_close(ctx->jackclient);
    // TODO destroy mpv, and other cleaning
    exit(EXIT_SUCCESS);
}

static
void
updateclip(struct context *ctx, struct keyval *kv)
{
    struct audioclip *ac;

    ac = &ctx->audioclip;

    if (strcmp(kv->key, "filepath") == 0)
        updatefilepath(ctx, kv);
    if (strcmp(kv->key, "position") == 0)
        updateposition(ac, kv);
    if (strcmp(kv->key, "info") == 0)
        printaudioclip(ac);
    if (strcmp(kv->key, "quit") == 0)
        quit(ctx);

    // TODO use a table?
}

static
void
obeyinput(struct context *ctx)
{
    struct keyval kv = {0};

    waitforinput();
    for (; readinput(&kv) == 0; )
        updateclip(ctx, &kv);
}

static
uint64_t
getjacktime(jack_client_t *jc)
{
    jack_position_t pos;

    jack_transport_query(jc, &pos);
    // TODO check 'valid'

    return (jack_time_t) pos.frame * 1000000 / pos.frame_rate;
    // TODO is this math safe from overflow?
}

static
int
isinpast(struct context *ctx)
{
    struct audioclip *ac;
    uint64_t usecs;

    ac = &ctx->audioclip;
    usecs = getjacktime(ctx->jackclient);

    return usecs >= ac->position;
}

static
int
isjackplaying(struct context *ctx)
{
    jack_transport_state_t state;

    state = jack_transport_query(ctx->jackclient, 0);

    return (state == JackTransportRolling);
}

static
void
playclip(struct mpv *mpv)
{
    mpv_set_property_string(mpv->handle, "pause", "no");
    mpv->playing = 1;
}

static
void
resetclip(struct mpv *mpv)
{
    mpv_set_property_string(mpv->handle, "pause", "yes");
    mpv_command_string(mpv->handle, "seek 0 absolute");
    mpv->playing = 0;
}

static
int
processjack(jack_nframes_t nf, void *context)
{
    struct context *ctx;
    struct mpv *mpv;

    (void)nf;
    ctx = context;
    mpv = &ctx->mpv;

    if (!mpv->playing && isjackplaying(ctx) && isinpast(ctx))
        playclip(mpv);
    if (mpv->playing && !isinpast(ctx))
        resetclip(mpv);

    return 0;
}

static
void
initjack(struct context *ctx)
{
    ctx->jackclient = jack_client_open("trak-timeplayer", JackNoStartServer, 0);
    if (!ctx->jackclient)
        die("failed to open jack client\n");

    jack_set_process_callback(ctx->jackclient, processjack, ctx);

    if (jack_activate(ctx->jackclient))
        die("can't activate jack client\n");

    // TODO cleanup
}

static
void
initmpv(struct context *ctx)
{
    mpv_handle *mpv;

    errno = 0;
    mpv = mpv_create();
    mpv_initialize(mpv);
    if (errno || !mpv)
        die("couldn't open mpv\n");

    mpv_set_property_string(mpv, "pause", "yes");
    mpv_set_property_string(mpv, "keep-open", "yes");
    ctx->mpv.handle = mpv;
    ctx->mpv.playing = 0;
    // TODO better error checking?
    // TODO mpv_terminate_destroy on exit (same for jack)
}



int
main(void)
{
    struct context ctx = {0};

    initjack(&ctx);
    initmpv(&ctx);

    for (;;)
        obeyinput(&ctx);

    return EXIT_FAILURE;
}
