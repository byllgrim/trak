#include <ctype.h>
#include <errno.h>
#include <mpv/client.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define LEN(a) (sizeof(a) / sizeof(a)[0])

enum {
    MAXTOKENS = 10,
    MAXBUFSIZ = 1000,
    MAXCLIPS = 1000, // TODO warn users or bigger value
    MAXNAMELEN = 100,
};

struct clip {
    char name[MAXNAMELEN];
    useconds_t start;
    useconds_t length;
    mpv_handle *mpv; // TODO hide backend specifics?
};

struct context {
    useconds_t playhead;
    struct clip clips[MAXCLIPS];
};

static void
die(char *msg)
{
    fprintf(stderr, msg);
    exit(EXIT_FAILURE);
}

static void
tokenize(char *str, int maxtok, char **tokens)
{
    int i;
    char *nl;

    for (; nl = strchr(str, '\n'); )
        *nl = ' ';

    i = 0;
    for (;;) {
        if (!str || !isprint(*str) || i == maxtok) {
            tokens[i] = 0;
            return;
        }

        if (isprint(*str) && !isblank(*str)) {
            tokens[i] = str;
            i++;
        }

        for (; isprint(*str) && !isblank(*str);)
            str++;

        for (; isblank(*str);) {
            *str = 0;
            str++;
        }
    }
}

static void
addclip(char *name, struct context *ctx)
{
    int i;
    mpv_handle *mpv;
    const char *cmd[3];
    mpv_event *me;
    struct clip *cl;

    // TODO refactor mpv shit out of here
    mpv = mpv_create();
    if (!mpv || mpv_initialize(mpv) < 0) {
        fprintf(stderr, "failed to create mpv context\n");
        return;
    }
    // TODO vo=null
    mpv_command_string(mpv, "set pause yes");
    cmd[0] = "loadfile";
    cmd[1] = name;
    cmd[2] = 0;
    mpv_command(mpv, cmd);
    for (;;) {
        me = mpv_wait_event(mpv, 10);
        if (me->event_id == MPV_EVENT_END_FILE || me->event_id == MPV_EVENT_NONE) {
            fprintf(stderr, "mpv failed to load file '%s'\n", name);
            mpv_destroy(mpv);
            return;
        }
        if (me->event_id == MPV_EVENT_FILE_LOADED)
            break;
    }

    for (i = 0; i < LEN(ctx->clips); i++) {
        cl = &ctx->clips[i];
        if (!cl->name[0]) {
            strncpy(cl->name, name, sizeof(cl->name));
            cl->start = 0; // TODO initialize to playhead?
            cl->length = 2000000; // TODO get actual length
            cl->mpv = mpv;

            return;
        }
    }
}

static void
printclips(struct context *ctx)
{
    int i;
    struct clip *cl;

    printf("playhead %d\n", ctx->playhead);

    for (i = 0; i < LEN(ctx->clips); i++) {
        cl = &ctx->clips[i];
        if (cl->name[0])
            printf("%d %s %d %d\n", i, cl->name, cl->start, cl->length);
    }

    fflush(stdout);
}

static void
playclips(struct context *ctx)
{
    int i;
    struct clip *cl;
    useconds_t next;
    struct timespec ts;

moretoplay:
    next = ctx->playhead;
    for (i = 0; i < LEN(ctx->clips); i++) {
        cl = &ctx->clips[i];
        if (!cl->mpv)
            continue;
        if (cl->start <= ctx->playhead)
            mpv_command_string(cl->mpv, "set pause no");
        else
            next = cl->start; // TODO find the shortest one
    }

    ts.tv_sec = (next - ctx->playhead) / 1000000;
    ts.tv_nsec = (next - ctx->playhead) % 1000000 * 1000;
    nanosleep(&ts, 0); // TODO this sleep can fail
    ctx->playhead = next;
    if (ts.tv_sec || ts.tv_nsec)
        goto moretoplay;
}

static void
moveclip(struct context *ctx, char *index, char *ustime)
{
    int i;
    useconds_t t;
    char *endptr;

    errno = 0;
    i = strtol(index, &endptr, 10);
    if (errno || endptr == index || i >= LEN(ctx->clips)) {
        // TODO generally print more error messages!
        // TODO if no clip at index?
        fprintf(stderr, "error in index of clip to move\n");
        return;
    }
    t = strtol(ustime, &endptr, 10);
    if (errno || endptr == ustime) {
        fprintf(stderr, "failed to convert µs time to move clip to\n");
        return;
    }

    ctx->clips[i].start = t;
}

static int
runcmd(char *cmd, struct context *ctx)
{
    char *tokens[MAXTOKENS + 1];

    tokenize(cmd, MAXTOKENS, tokens);

    if (!tokens[0])
        return 1;
    else if (!strcmp(tokens[0], "add") && tokens[1])
        addclip(tokens[1], ctx);
    else if (!strcmp(tokens[0], "move") && tokens[1] && tokens[2])
        moveclip(ctx, tokens[1], tokens[2]);
    else if (!strcmp(tokens[0], "play"))
        playclips(ctx);
    else if (!strcmp(tokens[0], "quit"))
        return 0;
    else
        return 1;

    printclips(ctx);
    return 1;
}

int
main(void)
{
    char buf[MAXBUFSIZ];
    struct context ctx;

    memset(buf, 0, sizeof(buf));
    memset(&ctx, 0, sizeof(ctx));
    for (; runcmd(buf, &ctx); )
        fgets(buf, sizeof(buf), stdin);

    return EXIT_SUCCESS;
}
