# trak

Audio track visualizer and manipulator in a single window.



# Data Structures

A track is:

     a list of clips

A clip is:

    clip {
        id:   number uniquely identifying the clip
        pos:  start position in micro seconds
        len:  duration in micro seconds
        file: path to the clip's associated file
    }

(TODO how to represent this structure among source files of different languages?)
